import React, { useState, useEffect } from "react";
import { Container, Row, Col, Form, Button, Spinner } from "react-bootstrap";
import { useParams, useNavigate } from "react-router-dom";
import Select from "react-select";
import { Formik } from "formik";
import * as yup from "yup";
import Swal from "sweetalert2";

import API from "../api/api";

const ConnexpayPaymentForm = () => {
    const [loader, setLoader] = useState(true);
    const [spinner, setSpinner] = useState(false);
    const [dataAliado, setDataAliado] = useState({});
    const [dataPayment, setDataPayment] = useState({});
    const [optionsCountries, setOptionsCountries] = useState([]);
    const [optionsStates, setOptionsStates] = useState([]);
    const [optionsCities, setOptionsCities] = useState([]);
    const [isLoadingCountries, setIsLoadingCountries] = useState(false);
    const [isLoadingStates, setIsLoadingStates] = useState(false);
    const [isLoadingCities, setIsLoadingCities] = useState(false);

    const { token } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        getDataPayment();
        getCountries();
    }, []);

    const schema = yup.object().shape({
        card: yup
            .string()
            .required()
            .test("test-number", "Credit Card number is invalid", value => {
                let sum = 0;
                let shouldDouble = false;
                // Elimina los espacios en blanco y los guiones
                const cardNumber = value.replace(/\s+/g, "").replace(/-/g, "");
                // Recorre los dígitos de derecha a izquierda
                for (let i = cardNumber.length - 1; i >= 0; i--) {
                    let digit = parseInt(cardNumber.charAt(i));

                    if (shouldDouble) {
                        digit *= 2;
                        if (digit > 9) {
                            digit -= 9;
                        }
                    }

                    sum += digit;
                    shouldDouble = !shouldDouble;
                }

                return sum % 10 === 0;
            }),

        cvc: yup
            .string()
            .required()
            .matches(/^\d{3,4}$/, "The CVC must have 3 or 4 digits"),
        expirationMonth: yup.number().required().min(1).max(12),
        expirationYear: yup.string().required().min(2).max(2),
        name: yup.string().required(),
        email: yup.string().required().email(),
        country: yup.object().shape({
            Name: yup.string().required(),
            Code: yup.string().required(),
        }),
        state: yup.object().required(),
        city: yup.object().required(),
        addressLine1: yup.string().required(),
        addressLine2: yup.string().required(),
        zip: yup.number().required(),
        termsAndUse: yup
            .bool()
            .required()
            .oneOf([true], "Terms must be accepted"),
    });

    const getDataPayment = async () => {
        try {
            setLoader(true);
            const ruta = `integraciones/connexpay/verificar_token/${token}`;
            const response = await API.get(ruta);
            setDataAliado(response.data.get_aliado);
            setDataPayment(response.data.get_link_pago);
        } catch (error) {
            console.error(error);
            navigate("/error");
        } finally {
            setLoader(false);
        }
    };

    const getCountries = async () => {
        try {
            setIsLoadingCountries(true);
            const ruta = "list_world_countries";
            const response = await API.get(ruta);

            if (dataAliado.tarjetas_americanas == "1") {
                response.data = response.data.filter(
                    el => el.Name == "United States" || el.Name == "Canada"
                );
            }

            await setOptionsCountries(response.data);
        } catch (error) {
            console.error(error);
        } finally {
            setIsLoadingCountries(false);
        }
    };

    const getStates = async (countryCode, selectedCity) => {
        try {
            setIsLoadingStates(true);
            const ruta = `list_world_country_privinces/${countryCode}`;
            const response = await API.get(ruta);

            const filteredStates = response.data.filter(
                stateFilter => stateFilter.Name === selectedCity.Province
            );

            setOptionsStates(response.data);

            if (filteredStates.length > 0) {
                return filteredStates[0];
            } else {
                return {};
            }
        } catch (error) {
            console.error(error);
        } finally {
            setIsLoadingStates(false);
        }
    };

    const handleCountryChange = async selectedOption => {
        try {
            setIsLoadingCities(true);
            const ruta = `list_world_privince_cities/${selectedOption.Code}/null`;
            const response = await API.get(ruta);
            setOptionsCities(response.data);
        } catch (error) {
            console.error(error);
        } finally {
            setIsLoadingCities(false);
            setOptionsStates([]);
        }
    };

    const handleSubmit = async (values, { setSubmitting }) => {
        try {
            setSpinner(true);
            const ruta =
                "integraciones/connexpay/procesar_pago/" + dataPayment.id;
            const params = {
                dataAliado: dataAliado,
                dataPayment: dataPayment,
                terms_and_use: values.termsAndUse,
                name: values.name,
                card: values.card,
                cvc: values.cvc,
                expirationYear: values.expirationYear,
                expirationMonth: values.expirationMonth,
                address_1: values.addressLine1,
                address_2: values.addressLine2,
                city: values.city,
                state: values.state,
                zip: values.zip,
                email: values.email,
            };

            await API.put(ruta, params);
            navigate("/successful", {
                state: { amount: dataPayment.amount },
            });
        } catch (error) {
            console.error(error);
            console.error(error.response);
            if (error.response) {
                if (error.response.status == 400) {
                    Swal.fire({
                        icon: "warning",
                        title: error.response.data,
                    });
                } else {
                    Swal.fire({
                        icon: "warning",
                        title: "Server Error",
                    });
                }
            } else {
                console.error(error.message);
            }
        } finally {
            setSubmitting(false);
            setSpinner(false);
        }
    };

    const initialValues = {
        termsAndUse: false,
        card: "",
        cvc: "",
        expirationMonth: "",
        expirationYear: "",
        name: "",
        addressLine1: "",
        addressLine2: "",
        country: null,
        state: null,
        city: null,
        zip: "",
        email: "",
    };

    return (
        <>
            {loader ? (
                <div className="text-center" style={{ marginTop: "30vh" }}>
                    <Spinner
                        animation="border"
                        role="status"
                        variant="primary"
                    ></Spinner>
                </div>
            ) : (
                <Row>
                    <Col xs="12" md="6" className="bordeDerecho pe-md-5">
                        <Row>
                            <Col>
                                <h5 className="text-secondary text-center text-md-start">
                                    Pay QUO VADIS TRAVEL AGENCY LLC
                                </h5>
                            </Col>
                        </Row>

                        <Row className="mt-2">
                            <Col>
                                <h1
                                    style={{ fontSize: "50px" }}
                                    className="text-center text-md-start"
                                >
                                    ${dataPayment.amount}
                                </h1>
                            </Col>
                        </Row>

                        <div className="price mt-2">
                            <Row className="py-2">
                                <Col>{dataPayment.description}</Col>
                                <Col className="text-end">
                                    ${dataPayment.amount}
                                </Col>
                            </Row>
                            <hr></hr>

                            <Row className="py-2">
                                <Col> Subtotal </Col>
                                <Col className="text-end">
                                    ${dataPayment.amount}
                                </Col>
                            </Row>
                            <hr></hr>

                            <Row className="py-2">
                                <Col> Total due </Col>
                                <Col className="text-end">
                                    ${dataPayment.amount}
                                </Col>
                            </Row>
                        </div>
                    </Col>

                    <Col xs="12" md="6" className="mt-5 mt-md-0 price">
                        <Container className="px-5">
                            <Formik
                                initialValues={initialValues}
                                validationSchema={schema}
                                onSubmit={handleSubmit}
                            >
                                {({
                                    handleSubmit,
                                    handleChange,
                                    handleBlur,
                                    setFieldValue,
                                    setFieldTouched,
                                    values,
                                    touched,
                                    errors,
                                }) => (
                                    <Form noValidate onSubmit={handleSubmit}>
                                        <Row className="pb-4">
                                            <Col className="text-center text-md-start">
                                                <h4>Pay with card</h4>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col
                                                className="mb-1"
                                                style={{ fontWeight: 500 }}
                                            >
                                                Card information
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs="12">
                                                <Form.Group>
                                                    <Form.Control
                                                        id="cardNumber"
                                                        placeholder="1234 1234 1234 1234"
                                                        size="sm"
                                                        name="card"
                                                        value={values.card}
                                                        onChange={handleChange}
                                                        isInvalid={errors.card}
                                                    ></Form.Control>
                                                    <Form.Control.Feedback
                                                        type="invalid"
                                                        className="mb-2"
                                                    >
                                                        {errors.card}
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                            </Col>
                                            <Col className="me-0 pe-0">
                                                <Form.Group>
                                                    <Form.Control
                                                        id="expirationMonth"
                                                        placeholder="MM"
                                                        size="sm"
                                                        value={
                                                            values.expirationMonth
                                                        }
                                                        name="expirationMonth"
                                                        onChange={handleChange}
                                                        isInvalid={
                                                            errors.expirationMonth
                                                        }
                                                    ></Form.Control>
                                                    <Form.Control.Feedback
                                                        type="invalid"
                                                        className="mb-2"
                                                    >
                                                        {errors.expirationMonth}
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                            </Col>
                                            <Col className="mx-0 px-0">
                                                <Form.Group>
                                                    <Form.Control
                                                        id="expirationYear"
                                                        placeholder="YY"
                                                        size="sm"
                                                        value={
                                                            values.expirationYear
                                                        }
                                                        name="expirationYear"
                                                        onChange={handleChange}
                                                        isInvalid={
                                                            errors.expirationYear
                                                        }
                                                    ></Form.Control>
                                                    <Form.Control.Feedback
                                                        type="invalid"
                                                        className="mb-2"
                                                    >
                                                        {errors.expirationYear}
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                            </Col>
                                            <Col className="ms-0 ps-0" cols="6">
                                                <Form.Group>
                                                    <Form.Control
                                                        id="cvc"
                                                        placeholder="CVC"
                                                        size="sm"
                                                        value={values.cvc}
                                                        name="cvc"
                                                        onChange={handleChange}
                                                        isInvalid={errors.cvc}
                                                    ></Form.Control>
                                                    <Form.Control.Feedback
                                                        type="invalid"
                                                        className="mb-2"
                                                    >
                                                        {errors.cvc}
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col
                                                className="mt-4 mb-1"
                                                style={{ fontWeight: 500 }}
                                            >
                                                Name on card
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col cols="12">
                                                <Form.Group>
                                                    <Form.Control
                                                        size="sm"
                                                        value={values.name}
                                                        name="name"
                                                        onChange={handleChange}
                                                        isInvalid={
                                                            !!errors.name
                                                        }
                                                    ></Form.Control>
                                                    <Form.Control.Feedback
                                                        type="invalid"
                                                        className="mb-2"
                                                    >
                                                        {errors.name}
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col
                                                className="mt-4 mb-1"
                                                style={{ fontWeight: 500 }}
                                            >
                                                Email
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col cols="12">
                                                <Form.Group>
                                                    <Form.Control
                                                        size="sm"
                                                        value={values.email}
                                                        name="email"
                                                        onChange={handleChange}
                                                        isInvalid={
                                                            !!errors.email
                                                        }
                                                    ></Form.Control>
                                                    <Form.Control.Feedback
                                                        type="invalid"
                                                        className="mb-2"
                                                    >
                                                        {errors.email}
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col
                                                className="mt-4 mb-1"
                                                style={{ fontWeight: 500 }}
                                            >
                                                Billing address
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs="12">
                                                <Form.Group>
                                                    <Select
                                                        options={
                                                            optionsCountries
                                                        }
                                                        getOptionValue={option =>
                                                            option.id
                                                        }
                                                        getOptionLabel={option =>
                                                            option.Name
                                                        }
                                                        isLoading={
                                                            isLoadingCountries
                                                        }
                                                        placeholder="Select a country"
                                                        value={values.country}
                                                        name="country"
                                                        onChange={option => {
                                                            setFieldValue(
                                                                "city",
                                                                null
                                                            );

                                                            setFieldValue(
                                                                "country",
                                                                option
                                                            );
                                                            handleCountryChange(
                                                                option
                                                            );
                                                        }}
                                                        onBlur={() =>
                                                            setFieldTouched(
                                                                "country",
                                                                true
                                                            )
                                                        }
                                                        styles={{
                                                            control:
                                                                baseStyles => ({
                                                                    ...baseStyles,
                                                                    borderColor:
                                                                        errors.country
                                                                            ? "red"
                                                                            : "#dee2e6",
                                                                }),
                                                        }}
                                                    />

                                                    <Form.Control.Feedback
                                                        type="invalid"
                                                        className="mb-2"
                                                        style={{
                                                            display:
                                                                errors.country
                                                                    ? "block"
                                                                    : "none",
                                                        }}
                                                    >
                                                        {errors.country}
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                            </Col>
                                            <Col xs="12">
                                                <Form.Group>
                                                    <Form.Control
                                                        placeholder="Address line 1"
                                                        size="sm"
                                                        value={
                                                            values.addressLine1
                                                        }
                                                        name="addressLine1"
                                                        onChange={handleChange}
                                                        isInvalid={
                                                            !!errors.addressLine1
                                                        }
                                                    ></Form.Control>
                                                    <Form.Control.Feedback
                                                        type="invalid"
                                                        className="mb-2"
                                                    >
                                                        {errors.addressLine1}
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                            </Col>
                                            <Col xs="12">
                                                <Form.Group>
                                                    <Form.Control
                                                        placeholder="Address line 2"
                                                        size="sm"
                                                        value={
                                                            values.addressLine2
                                                        }
                                                        name="addressLine2"
                                                        onChange={handleChange}
                                                        isInvalid={
                                                            !!errors.addressLine2
                                                        }
                                                    ></Form.Control>
                                                    <Form.Control.Feedback
                                                        type="invalid"
                                                        className="mb-2"
                                                    >
                                                        {errors.addressLine2}
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col
                                                xs="12"
                                                md="6"
                                                className="me-md-0 pe-md-0"
                                            >
                                                <Form.Group>
                                                    <Select
                                                        value={values.city}
                                                        name="city"
                                                        options={optionsCities}
                                                        getOptionValue={option =>
                                                            option.id
                                                        }
                                                        getOptionLabel={option =>
                                                            option.Name
                                                        }
                                                        isLoading={
                                                            isLoadingCities
                                                        }
                                                        isDisabled={
                                                            isLoadingCities
                                                        }
                                                        placeholder="Select a city"
                                                        onChange={async option => {
                                                            setFieldValue(
                                                                "city",
                                                                option
                                                            );

                                                            const valueState =
                                                                await getStates(
                                                                    values
                                                                        .country
                                                                        .Code,
                                                                    option
                                                                );

                                                            setFieldValue(
                                                                "state",
                                                                valueState
                                                            );
                                                        }}
                                                        onBlur={() =>
                                                            setFieldTouched(
                                                                "city",
                                                                true
                                                            )
                                                        }
                                                        styles={{
                                                            control:
                                                                baseStyles => ({
                                                                    ...baseStyles,
                                                                    borderColor:
                                                                        errors.city
                                                                            ? "red"
                                                                            : "#dee2e6",
                                                                }),
                                                        }}
                                                    />
                                                    <Form.Control.Feedback
                                                        type="invalid"
                                                        className="mb-2"
                                                        style={{
                                                            display: errors.city
                                                                ? "block"
                                                                : "none",
                                                        }}
                                                    >
                                                        {errors.city}
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                            </Col>
                                            <Col
                                                xs="12"
                                                md="6"
                                                className="ms-md-0 ps-md-0"
                                            >
                                                <Form.Group>
                                                    <Form.Control
                                                        placeholder="ZIP"
                                                        size="sm"
                                                        value={values.zip}
                                                        name="zip"
                                                        onChange={handleChange}
                                                        isInvalid={!!errors.zip}
                                                    ></Form.Control>
                                                    <Form.Control.Feedback
                                                        type="invalid"
                                                        className="mb-2"
                                                    >
                                                        {errors.zip}
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs="12">
                                                <Form.Group>
                                                    <Select
                                                        value={values.state}
                                                        name="state"
                                                        options={optionsStates}
                                                        getOptionValue={option =>
                                                            option.Name
                                                        }
                                                        getOptionLabel={option =>
                                                            option.Name
                                                        }
                                                        isLoading={
                                                            isLoadingStates
                                                        }
                                                        isDisabled={true}
                                                        placeholder="Select a state"
                                                        isInvalid={
                                                            !!errors.state
                                                        }
                                                    />
                                                    <Form.Control.Feedback
                                                        type="invalid"
                                                        className="mb-2"
                                                    >
                                                        {errors.state}
                                                    </Form.Control.Feedback>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row className="mt-4">
                                            <Col>
                                                <Form.Group>
                                                    <Form.Check>
                                                        <Form.Check.Input
                                                            type="checkbox"
                                                            checked={
                                                                values.termsAndUse
                                                            }
                                                            name="termsAndUse"
                                                            onChange={
                                                                handleChange
                                                            }
                                                            isInvalid={
                                                                errors.termsAndUse
                                                            }
                                                        />
                                                        <Form.Check.Label>
                                                            I accept the{" "}
                                                            <a
                                                                href="https://sisqv.quovadistravel.com/sisqv/Backend/public/termsAndConditionsQVTA/TERMS%20AND%20CONDITIONS%20QVTA.pdf"
                                                                target="_blank"
                                                                rel="noopener noreferrer"
                                                                style={{
                                                                    color: "blue",
                                                                }}
                                                            >
                                                                terms and
                                                                conditions
                                                            </a>
                                                        </Form.Check.Label>

                                                        <Form.Control.Feedback
                                                            type="invalid"
                                                            className="mb-2"
                                                        >
                                                            {errors.termsAndUse}
                                                        </Form.Control.Feedback>
                                                    </Form.Check>
                                                </Form.Group>
                                            </Col>
                                        </Row>
                                        <Row className="mt-3">
                                            <Col className="d-grid gap-2">
                                                <Button
                                                    size="sm"
                                                    disabled={
                                                        spinner ||
                                                        !values.termsAndUse
                                                    }
                                                    type="sumbit"
                                                >
                                                    {spinner ? (
                                                        <Spinner
                                                            as="span"
                                                            animation="border"
                                                            size="sm"
                                                            role="status"
                                                            aria-hidden="true"
                                                        />
                                                    ) : (
                                                        <span> Pay </span>
                                                    )}
                                                </Button>
                                            </Col>
                                        </Row>
                                        <Row className="mt-3">
                                            <Col>
                                                <p
                                                    className="text-muted px-5 text-center text-md-left"
                                                    style={{
                                                        fontSize: "12px",
                                                    }}
                                                >
                                                    By confirming your payment,
                                                    you allow QUO VADIS TRAVEL
                                                    AGENCY LLC to charge your
                                                    card for this payment
                                                </p>
                                            </Col>
                                        </Row>
                                    </Form>
                                )}
                            </Formik>
                        </Container>
                    </Col>
                </Row>
            )}
        </>
    );
};

export default ConnexpayPaymentForm;
