import React from "react";
import { Row, Col } from "react-bootstrap";

import imgError from "../assets/11132.webp";

const ConnexpayPaymentFormError = () => {
    let mostrarAlerta = "¡Invalid payment link!";

    return (
        <Row className="text-center text-md-start d-flex align-items-center">
            <Col xs="12" md="5">
                <Row>
                    <Col>
                        <h3 className="titlePaymentError">¡Error!</h3>
                    </Col>
                </Row>
                <Row>
                    <Col style={{ fontSize: "30px" }}>{mostrarAlerta}</Col>
                </Row>
                <Row className="mt-4">
                    <Col style={{ fontSize: "15px" }}>
                        If you have any questions please contact your sales
                        advisor.
                    </Col>
                </Row>
            </Col>
            <Col style={{ maxWidth: "100%" }} className="mt-5 mt-md-0">
                <img
                    src={imgError}
                    alt="error-image"
                    style={{ width: "100%" }}
                ></img>
            </Col>
        </Row>
    );
};

export default ConnexpayPaymentFormError;
