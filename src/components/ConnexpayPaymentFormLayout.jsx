import React from "react";
import { Outlet } from "react-router-dom";
import { Container, Row, Col, Image } from "react-bootstrap";
import logoQvTravel from "../assets/QuoVadis-200px.png";

const ConnexpayPaymentFormLayout = () => {
    return (
        <Container className="py-md-4 px-md-5">
            <Container>
                <Row className="py-4">
                    <Col>
                        <Image src={logoQvTravel} width="150" />
                    </Col>
                </Row>
                <Outlet />
            </Container>
        </Container>
    );
};

export default ConnexpayPaymentFormLayout;
