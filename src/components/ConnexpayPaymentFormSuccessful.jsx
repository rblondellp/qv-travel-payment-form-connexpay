import React from "react";
import { useLocation } from "react-router-dom";
import { Row, Col } from "react-bootstrap";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle } from "@fortawesome/free-regular-svg-icons";

const ConnexpayPaymentFormSuccessful = () => {
    const location = useLocation();
    const { amount } = location.state;

    return (
        <Row className="text-center mt-5">
            <Col
                xs="12"
                md={{ span: 6, offset: 3 }}
                lg={{ span: 4, offset: 4 }}
            >
                <Row>
                    <Col>
                        <FontAwesomeIcon
                            icon={faCheckCircle}
                            className="checkIcon"
                        />
                    </Col>
                </Row>
                <Row className="mt-5">
                    <Col>
                        <h3 className="titlePaymentSuccessful">
                            Thanks for your payment
                        </h3>
                    </Col>
                </Row>
                <Row className="mt-2">
                    <Col>
                        <span
                            className="text-secondary"
                            style={{ fontWeight: 500 }}
                        >
                            A payment to quovadistravel.com will appear on your
                            statement.
                        </span>
                    </Col>
                </Row>
                <Row
                    className="mt-5 pt-4 pb-4 p-md-4"
                    style={{ backgroundColor: "#f5f5f5", borderRadius: "5px" }}
                >
                    <Col className="text-secondary" style={{ fontWeight: 600 }}>
                        <Row>
                            <Col> QUOVADISTRAVEL.COM </Col>
                            <Col className="text-right"> ${amount} </Col>
                        </Row>
                    </Col>
                </Row>
            </Col>
        </Row>
    );
};

export default ConnexpayPaymentFormSuccessful;
