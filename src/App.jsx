import { createBrowserRouter, RouterProvider } from "react-router-dom";
import ErrorPage from "./components/ErrorPage";
import ConnexpayPaymentFormLayout from "./components/ConnexpayPaymentFormLayout";
import ConnexpayPaymentForm from "./components/ConnexpayPaymentForm";
import ConnexpayPaymentFormError from "./components/ConnexpayPaymentFormError";
import ConnexpayPaymentFormSuccessful from "./components/ConnexpayPaymentFormSuccessful";

const router = createBrowserRouter(
    [
        {
            path: "/",
            element: <ConnexpayPaymentFormLayout />,
            errorElement: <ErrorPage />,
            children: [
                {
                    path: "/:token",
                    element: <ConnexpayPaymentForm />,
                },
                {
                    path: "/error",
                    element: <ConnexpayPaymentFormError />,
                },
                {
                    path: "/successful",
                    element: <ConnexpayPaymentFormSuccessful />,
                },
            ],
        },
    ],
    { basename: "/payment_form" }
);

function App() {
    return <RouterProvider router={router} />;
}

export default App;
